 var express  = require('express');
    var app      = express();
    var mongoose = require('mongoose');
    var morgan = require('morgan');
    var bodyParser = require('body-parser');
    var methodOverride = require('method-override');

    mongoose.connect('mongodb://localhost/test1')

    app.use(express.static(__dirname + '/public'));
    app.use(morgan('dev'));
    app.use(bodyParser.urlencoded({'extended':'true'}));
    app.use(bodyParser.json());
    app.use(bodyParser.json({ type: 'application/vnd.api+json' }));
    app.use(methodOverride());

    var Customer = mongoose.model('Customer', {
        name : { type: String, required: true },
        email : { type: String, required: true },
        telephone : { type: String, required: true },
        address : { type: String, required: true },
        street : String,
        city : String,
        state : String,
        strzip : String
    });

    app.get('/api/customers', function(req, res) {
        Customer.find(function(err, customers) {
            if (err)res.send(err);
            res.json(customers);
        });
    });
    
    app.post('/api/create_customer', function(req, res) {
        Customer.create({
            name : req.body.name,
            email : req.body.email,
            telephone : req.body.telephone,
            address : req.body.address,
            street : req.body.street,
            city : req.body.city,
            state : req.body.state,
            strzip : req.body.strzip
        }, function(err, customer) {
            if (err)res.send(err);

            Customer.find(function(err, customers) {
                if (err)res.send(err);
                res.json(customers);
            });
        });

    });

    app.delete('/api/delete_customer/:customer_id', function(req, res) {
        Customer.remove({
            _id : req.params.customer_id
        }, function(err, customer) {
            if (err)res.send(err);

            Customer.find(function(err, customers) {
                if (err)res.send(err);
                res.json(customers);
            });
        });
    });

    app.put('/api/update_customer/', function(req, res) {
        Customer.findByIdAndUpdate(req.body._id, { 
            $set: {             
                    name : req.body.name,
                    email : req.body.email,
                    telephone : req.body.telephone,
                    address : req.body.address,
                    street : req.body.street,
                    city : req.body.city,
                    state : req.body.state,
                    strzip : req.body.strzip}
                }
            ,function (err, customer) {
              if (err) return handleError(err);
            Customer.find(function(err, customers) {
                if (err)res.send(err);
                res.json(customers);
            });
        });
    });

    app.get('*', function(req, res) {
        res.sendfile('./public/index.html');
    });

    app.listen(8080);
    console.log("App listening on port 8080");