var testApp = angular.module('testApp', ['ngRoute']);

testApp.controller('testAppController',['$scope', '$rootScope','$http','customersService', function ($scope, $rootScope, $http,customersService){
  $scope.handleSuccess = function(data, status) {
        $scope.customers = data;
  };
  $scope.getCustomers = function(){
    customersService.get().success($scope.handleSuccess);
  };

  $scope.requestModel = {
        name : "",
        email : "",
        telephone : "",
        address : "",
        street : "",
        city : "",
        state : "",
        strzip : ""
  };

  $scope.showModal = false;

  $scope.toggleModal = function(c){
    $scope.showModal = !$scope.showModal;
    $scope.editCustomerModel = c;
  };

  $scope.deleteCustomer = function(id){
    customersService.delete(id).success($scope.handleSuccess);
  };

  $scope.addCustomer = function(){
    console.log($scope.requestModel);
    customersService.create($scope.requestModel).success($scope.handleSuccess);
    $scope.requestModel = {};
  };

  $scope.updateCustomer = function(editCustomerModel){
    customersService.update(editCustomerModel).success($scope.handleSuccess);
  };
}]);

testApp.directive('modal', function () {
    return {
      templateUrl: '/views/modal.html',
      restrict: 'E',
      transclude: true,
      replace:true,
      scope:true,
      link: function postLink(scope, element, attrs) {
        scope.$watch(attrs.visible, function(value){
          if(value == true)
            $(element).modal('show');
          else
            $(element).modal('hide');
        });

        $(element).on('shown.bs.modal', function(){
          scope.$apply(function(){
            scope.$parent[attrs.visible] = true;
          });
        });

        $(element).on('hidden.bs.modal', function(){
          scope.$apply(function(){
            scope.$parent[attrs.visible] = false;
          });
        });
      }
    };
});

testApp.factory('customersService', function($http) {
        return {
            get : function() {
              return $http.get('/api/customers');
            },
            create : function(requestModel){
              return $http.post('/api/create_customer', requestModel);
            },
            delete : function(id){
              return $http.delete('/api/delete_customer/' + id);
            },
            update : function(editCustomerModel){
              return $http.put('/api/update_customer', editCustomerModel)
            }
        }
});

